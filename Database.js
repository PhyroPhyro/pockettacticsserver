if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
        });
    };
}

var fs = require('fs')
var mysql = require('mysql');
var Utils = require('./Utils');
var GCM = require('./GCM');
var crypto = require('crypto');
var log4js = require('log4js');

//Logger
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('logs/general.log'), 'general');
var loggedPlayers = [];

// Define our db creds
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'pocketactics'
})

exports.db = db;

// Log any errors connected to the db
db.connect(function (err) {
    if (err) log4js.getLogger('general').error(err)
    else log4js.getLogger('general').info("Server connected")
})

exports = module.exports = function (io) {

    io.sockets.on('connection', function (socket) {

        var sendJson = { message: socket.id };
        socket.emit("SetClientID", sendJson);

        //Setup DB
        var queryFromDB = function(query, callback) {
            db.query(query, function (error, result) {
                if (error) {
                    log4js.getLogger('general').info(error);
                    return;
                }

                if (result)
                    callback(result);
            });
        }
        exports.queryFromDB = queryFromDB;

        var getUserInfo = function(obj)
        {
            return io.sockets.connected[obj.clientID];
        }
        exports.getUserInfo = getUserInfo;

        var userLogger = {};
        userLogger.info = function (userID, msg) {
            log4js.getLogger('player' + userID).info(msg);
        }
        userLogger.trace = function (userID, msg) {
            log4js.getLogger('player' + userID).trace(msg);
        }
        userLogger.warn = function (userID, msg) {
            log4js.getLogger('player' + userID).warn(msg);
        }
        userLogger.error = function (userID, msg) {
            log4js.getLogger('player' + userID).error(msg);
        }
        exports.userLogger = userLogger;

        //Error handling
        process.on('uncaughtException', function (err) {
            log4js.getLogger('general').info(err.stack);
            process.exit();
        });

        ////////
        //MOTD//
        ////////
        socket.on('requestMOTD', function (obj) {
            var motdString = fs.readFileSync('MOTD.txt').toString();
            var sendJson = { message: motdString };
            io.to(obj.clientID).emit("DefaultCallback", sendJson);
        })

        ///////////
        //ACCOUNT//
        ///////////
        var currentMaxTroops = 30;

        socket.on('verifyGoogleAccount', function (obj) {
            log4js.getLogger('general').info("Verifying google id: " + obj.id);
            queryFromDB(String.format("SELECT * FROM users WHERE googleID='{0}'", obj.id), function (result) {
                if (result.length > 0) {
                    loginUser(result[0].id, obj);
                } else {
                    setupNewAccount(obj);
                }
            })
        })

        socket.on('submitAccountNickname', function (obj) {
            exports.getUserByToken(obj.token, function (user) {
                var queryString = String.format("UPDATE users SET name='{0}' WHERE id={1}", obj.name, user.id);
                queryFromDB(queryString, function (result) {
                    log4js.getLogger('general').info("Name " + obj.name + " submited to player id " + user.id);
                    var queryString = String.format("SELECT * FROM users WHERE id='{0}'", user.id);
                    queryFromDB(queryString, function (result) {
                        io.to(obj.clientID).emit("DefaultCallback", result[0]);
                    })
                })
            })

        })

        socket.on('submitRegistrationID', function (obj) {
            exports.getUserByToken(obj.token, function (user) {
                var queryString = String.format("UPDATE users SET registrationID='{0}' WHERE id={1}", obj.regID, user.id);
                queryFromDB(queryString, function (result) {
                    log4js.getLogger('general').info("Reg ID " + obj.regID + " submited to player id " + user.id);
                })
            });
        })

        var setupNewAccount = function(obj) {
            log4js.getLogger('general').info("Creating account with google id: " + obj.id);
            var queryString = String.format("INSERT INTO users (googleID) VALUES ('{0}')", obj.id);
            queryFromDB(queryString, function (result) {

                var newArmoryString = String.format("INSERT INTO armory (playerID,troopLeader,troopTank,troopMelee,troopRanged,troopFlying,troopBuilding) VALUES ('{0}',1,2,3,4,5,6)", result.insertId);
                queryFromDB(newArmoryString, function (newArmoryResult) {
                    if (newArmoryResult.insertId)
                    {
                        log4js.getLogger('general').info("Created new armory for id " + result.insertId);
                        var queryString = String.format("SELECT * FROM users WHERE id='{0}'", result.insertId);
                        queryFromDB(queryString, function (result) {
                            getUserInfo(obj).currentUserObj = result[0];

                            addLootToInventory(obj, 1, "troopLeader", 2, 1);
                            addLootToInventory(obj, 2, "troop1", 2, 1);
                            addLootToInventory(obj, 3, "troop2", 2, 1);
                            addLootToInventory(obj, 4, "troop3", 2, 1);
                            addLootToInventory(obj, 5, "troop4", 2, 1);
                            addLootToInventory(obj, 6, "troop5", 2, 1);
                            log4js.getLogger('general').info("Added inventory troops for id " + result[0].id);

                            addLootToInventory(obj, 1, "PlayerMatches", Utils.LOOT_TYPES.MATCHES.value, 3);
                            log4js.getLogger('general').info("Added inventory matches for id " + result[0].id);

                            addLootToInventory(obj, 1, "PlayerCurrency", Utils.LOOT_TYPES.CURRENCY.value, 0);
                            log4js.getLogger('general').info("Added inventory currency for id " + result[0].id);

                            var newLootBoxString = String.format("INSERT INTO lootbox (playerID,counterSinceLastTroop) VALUES ('{0}', 0)", result[0].id);
                            queryFromDB(newLootBoxString, function (newLootBoxResult) {
                                addLootToInventory(obj, 1, "LootBox", Utils.LOOT_TYPES.BOX.value, 0);
                                log4js.getLogger('general').info("Added lootbox to new player");
                            })

                            loginUser(result[0].id, obj);
                        })

                    } else {
                        var sendJson = { message: "Error creating your armory" }
                        io.to(obj.clientID).emit("DefaultCallback", sendJson);
                        return;
                    }
                })
            })
        }

        var loginUser = function(userID, obj)
        {
            var token = crypto.randomBytes(20).toString('hex');
            var queryString = String.format("UPDATE users SET token='{0}' WHERE id={1}", token, userID);
            queryFromDB(queryString, function (result) {

                //Register player log
                if (loggedPlayers.indexOf(userID) == -1)
                {
                    log4js.addAppender(log4js.appenders.file('logs/player' + userID + '.log'), 'player' + userID);
                    loggedPlayers.push(userID);
                }

                exports.userLogger.info(userID, "Token " + token + " submited to player id " + userID);
                queryFromDB(String.format("SELECT * FROM users WHERE id='{0}'", userID), function (result) {
                    if (result.length > 0) {
                        getUserInfo(obj).currentUserObj = result[0];
                        exports.userLogger.info(userID, "Returning user: " + result[0].id)
                        io.to(obj.clientID).emit("DefaultCallback", result[0]);
                    }
                })
            })
        }

        exports.getUserByToken = function(token, callback)
        {
            var queryString = String.format("SELECT * FROM users WHERE token='{0}'", token);
            queryFromDB(queryString, function (result) {
                callback(result[0]);
            })
        }

        /////////////
        ////RANKS////
        /////////////
        socket.on('requestMyRanks', function (obj) {
            var currentGlobalRank;
            var currentHonorRank;
            var nextGlobalRankValue;
            var nextHonorRankValue;

            if (getUserInfo(obj).currentUserObj.globalRankExp >= Utils.GLOBAL_RANK.GOLD.value) {
                nextGlobalRankValue = 0;
                currentGlobalRank = Utils.GLOBAL_RANK.GOLD.name;
            }
            else if (getUserInfo(obj).currentUserObj.globalRankExp >= Utils.GLOBAL_RANK.SILVER.value) {
                nextGlobalRankValue = Utils.GLOBAL_RANK.GOLD.value;
                currentGlobalRank = Utils.GLOBAL_RANK.SILVER.name;
            }
            else if (getUserInfo(obj).currentUserObj.globalRankExp >= Utils.GLOBAL_RANK.BRONZE.value) {
                nextGlobalRankValue = Utils.GLOBAL_RANK.SILVER.value;
                currentGlobalRank = Utils.GLOBAL_RANK.BRONZE.name;
            }

            if (getUserInfo(obj).currentUserObj.honorRankExp >= Utils.HONOR_RANK.COMMANDER.value) {
                nextHonorRankValue = 0;
                currentHonorRank = Utils.HONOR_RANK.COMMANDER.name;
            }
            else if (getUserInfo(obj).currentUserObj.honorRankExp >= Utils.HONOR_RANK.GENERAL.value) {
                nextHonorRankValue = Utils.HONOR_RANK.COMMANDER.value;
                currentHonorRank = Utils.HONOR_RANK.GENERAL.name;
            }
            else if (getUserInfo(obj).currentUserObj.honorRankExp >= Utils.HONOR_RANK.CABLE.value) {
                nextHonorRankValue = Utils.HONOR_RANK.GENERAL.value;
                currentHonorRank = Utils.HONOR_RANK.CABLE.name;
            }
            
            var sendJson = {
                _globalRankExp: getUserInfo(obj).currentUserObj.globalRankExp,
                _honorRankExp: getUserInfo(obj).currentUserObj.honorRankExp,
                _currentGlobalRank: currentGlobalRank,
                _currentHonorRank: currentHonorRank,
                _nextGlobalRankValue: nextGlobalRankValue,
                _nextHonorRankValue: nextHonorRankValue
            }

            io.to(obj.clientID).emit("UpdatePlayerRanks", sendJson);
        })

        /////////////////
        ////INVENTORY////
        /////////////////
        socket.on('requestMyInventory', function (obj) {
            sendPlayerInventory(obj);
        })

        var addLootToInventory = function(obj, _id, _name, _type, _amount, callback)
        {
            if (getUserInfo(obj).currentUserInventory)
            {
                for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                    if (getUserInfo(obj).currentUserInventory[i]._lootType == _type && getUserInfo(obj).currentUserInventory[i]._lootType != Utils.LOOT_TYPES.TROOP.value)
                    {
                        exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Updating loot to player " + getUserInfo(obj).currentUserObj.id + " inventory");
                        var updateMatchQueryString = String.format("UPDATE inventory SET lootAmount={0} WHERE id={1}", (getUserInfo(obj).currentUserInventory[i]._lootAmount + _amount), getUserInfo(obj).currentUserInventory[i]._id);
                        queryFromDB(updateMatchQueryString, function (updateResult) {
                            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Match loot improved by " + _amount);
                            if (callback != null)
                                callback();
                        })

                        return;
                    }
                }
            }

            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Adding loot to player " + getUserInfo(obj).currentUserObj.id + " inventory");
            var queryString = String.format("INSERT INTO `inventory` (`lootName`, `lootType`, `lootAmount`, `lootID`, `playerID`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}');", _name, _type, _amount, _id, getUserInfo(obj).currentUserObj.id);
            queryFromDB(queryString, function (result) {
                if (result.insertId) {
                    if(callback != null)
                        callback();
                } else {
                    exports.userLogger.error(getUserInfo(obj).currentUserObj.id, "Error on inserting loot: " + _name + " to player " + getUserInfo(obj).currentUserObj.id);
                }
            })
        }

        var sendPlayerInventory = function(obj)
        {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending inventory to player: " + getUserInfo(obj).currentUserObj.id);
            var queryString = String.format("SELECT * FROM inventory WHERE inventory.playerID = {0}", getUserInfo(obj).currentUserObj.id);
            queryFromDB(queryString, function (result) {
                var sendJson = {};
                if (result.length > 0) {
                    getUserInfo(obj).currentUserInventory = [];
                    for (i in result) {
                        getUserInfo(obj).currentUserInventory.push(new Utils.LootData(result[i].id, result[i].lootName, result[i].lootType, result[i].lootAmount, result[i].lootID, result[i].playerID, result[i].timeUpdated));
                    }

                    checkMatchesAmount(obj, function () {
                        for (i in getUserInfo(obj).currentUserInventory)
                        {
                            io.to(obj.clientID).emit("UpdatePlayerInventory", getUserInfo(obj).currentUserInventory[i]);
                        }

                        sendJson = { message: "done" }
                        io.to(obj.clientID).emit("UpdatePlayerInventory", sendJson);
                    })

                } else {
                    sendJson = { message: "Error on getting new inventory from database" }
                    io.to(obj.clientID).emit("UpdatePlayerInventory", sendJson);
                }
            })
        }

        var checkMatchesAmount = function(obj, callback)
        {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Checking matches for player: " + getUserInfo(obj).currentUserObj.id);
            for (i in getUserInfo(obj).currentUserInventory)
            {
                if(getUserInfo(obj).currentUserInventory[i]._lootType == Utils.LOOT_TYPES.MATCHES.value)
                {
                    if(getUserInfo(obj).currentUserInventory[i]._lootAmount < 3)
                    {
                        var past = getUserInfo(obj).currentUserInventory[i]._timeUpdated.getTime();
                        var matchThresholdMinutes = 1000 * 60 * 30;
                        var isPast = (new Date().getTime() - past < matchThresholdMinutes) ? false : true;
                        if (isPast)
                        {
                            addLootToInventory(obj, 1, "PlayerMatches", Utils.LOOT_TYPES.MATCHES.value, 1, function () {
                                sendPlayerInventory(obj);
                            });
                        } else {
                            callback();
                        }
                        return;
                    }
                }
            }

            callback();
        }

        ////////////
        ////LOOT////
        ////////////
        socket.on('openLootBox', function (obj) {
            //Verifying
            var invObj;
            for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                if (getUserInfo(obj).currentUserInventory[i]._lootType == Utils.LOOT_TYPES.BOX.value)
                {
                    invObj = getUserInfo(obj).currentUserInventory[i];
                    if (getUserInfo(obj).currentUserInventory[i]._lootAmount <= 0)
                    {
                        exports.userLogger.error(getUserInfo(obj).currentUserObj.id, "There isn't any boxes to be opened");
                        return;
                    }
                }
            }

            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Opening box for player: " + getUserInfo(obj).currentUserObj.id);
            var updateQueryString = String.format("UPDATE inventory SET lootAmount={0} WHERE id='{1}'", (invObj._lootAmount - 1), invObj._id);
            queryFromDB(updateQueryString , function (result) {
                var queryString = String.format("SELECT * FROM lootbox WHERE playerID = {0}", getUserInfo(obj).currentUserObj.id);
                queryFromDB(queryString, function (result) {
                    var troopChancesInit = 0.05;
                    var troopChancesMod = result[0].counterSinceLastTroop/5;
                    var troopChances = troopChancesInit * troopChancesMod;
                    
                    var currentTroopsAmount = 0;
                    for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                        if (getUserInfo(obj).currentUserInventory[i]._lootType == Utils.LOOT_TYPES.TROOP.value)
                            currentTroopsAmount++;
                    }
                    var d = Math.random();
                    if (d < troopChances && currentTroopsAmount < currentMaxTroops)
                    {
                        addNewTroopToInventory(obj);
                        updateUserLootBox(obj, 0);
                    }
                    else
                    {
                        addNewPackToInventory(obj);
                        updateUserLootBox(obj, (result[0].counterSinceLastTroop + 1));
                    }
                })
            })
        })

        var updateUserLootBox = function (obj, newCounter) {
            var updateQueryString = String.format("UPDATE lootbox SET counterSinceLastTroop={0} WHERE playerID='{1}'", newCounter, getUserInfo(obj).currentUserObj.id);
            queryFromDB(updateQueryString, function (result) { exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "LootBox troop counter updated") })
        }

        var addNewTroopToInventory = function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Add new sorted troop to player");
            var queryString = String.format("SELECT * FROM troops WHERE isEquipable=1");
            queryFromDB(queryString, function (result) {
                var randomTroop;
                var hasTroop = true;
                while (hasTroop)
                {
                    randomTroop = result[Utils.getRandomInt(0, result.length - 1)];
                    for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                        if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === randomTroop.id }).length <= 0) hasTroop = false;
                    }
                }
                addLootToInventory(obj, randomTroop.id, randomTroop.name, Utils.LOOT_TYPES.TROOP.value, 1, function () { io.to(obj.clientID).emit("DefaultCallback", randomTroop); });
            })
        }

        var addNewPackToInventory = function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Add new sorted pack to player");
            var queryString = String.format("SELECT * FROM lootpacks");
            queryFromDB(queryString, function (result) {
                var randomPack = result[Utils.getRandomInt(0, result.length - 1)];
                addLootToInventory(obj, randomPack.id, randomPack.name, randomPack.lootType, randomPack.lootAmount, function () { io.to(obj.clientID).emit("DefaultCallback", randomPack); });
            })
        }

        ///////////////
        ////CREDITS////
        ///////////////
        socket.on('requestBuyTroop', function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Player " + getUserInfo(obj).currentUserObj.id + " trying to buy troop " + obj.troopID);

            var userCurrency;
            for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.troopID }).length > 0)
                {
                    exports.userLogger.error(getUserInfo(obj).currentUserObj.id, "Player already has troop");
                    sendJson = { error: "You already have this troop." };
                    io.to(obj.clientID).emit("DefaultCallback", sendJson);
                }

                if (getUserInfo(obj).currentUserInventory[i]._lootType == Utils.LOOT_TYPES.CURRENCY.value)
                    userCurrency = getUserInfo(obj).currentUserInventory[i]._lootAmount;
            }

            var queryString = String.format("SELECT * FROM troops WHERE troops.id = {0}", obj.troopID);
            queryFromDB(queryString, function (result) {
                if (result.length > 0) {
                    if(userCurrency < result[0].buyCost)
                    {
                        exports.userLogger.warn(getUserInfo(obj).currentUserObj.id, "Not enough credits");
                        sendJson = { error: "Not enough credits to buy this troop." };
                        io.to(obj.clientID).emit("DefaultCallback", sendJson);
                    } else {
                        addLootToInventory(obj, result[0].id, result[0].name, Utils.LOOT_TYPES.TROOP.value, 1, function () {
                            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Bought " + result[0].name + " successfully.");
                            sendJson = { message: "Bought " + result[0].name + " successfully." };

                            addLootToInventory(obj, 1, "PlayerCurrency", Utils.LOOT_TYPES.CURRENCY.value, -result[0].buyCost, function () {
                                io.to(obj.clientID).emit("DefaultCallback", sendJson);
                            })
                        });
                    }
                }
            })
        })

        ////////////
        ///ARMORY///
        ////////////
        socket.on('requestMyArmory', function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending armory to player: " + getUserInfo(obj).currentUserObj.id);
            var queryString = String.format("SELECT * FROM troops WHERE troops.id = (SELECT troopLeader FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopTank FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopMelee FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopRanged FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopFlying FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopBuilding FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1)", getUserInfo(obj).currentUserObj.id);
            queryFromDB(queryString, function (result) {
                if (result.length > 0) {
                    for (i in result) {
                        io.to(obj.clientID).emit("UpdatePlayerArmory", result[i]);
                    }
                }
            })
        })

        socket.on('requestArmoryTroops', function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending all armory troops to playerID: " + getUserInfo(obj).currentUserObj.id);
            var queryString = "SELECT * FROM troops WHERE isEquipable=1";
            queryFromDB(queryString, function (result) {
                if (result.length > 0) {
                    for (i in result) {
                        io.to(obj.clientID).emit("UpdateArmoryTroops", result[i]);
                    }
                    var sendJson = { message: "done" }
                    io.to(obj.clientID).emit("UpdateArmoryTroops", sendJson);
                }
            })
        })

        socket.on('requestArmorySave', function (obj) {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Verifying new armory from playerID: " + getUserInfo(obj).currentUserObj.id);
            var canSave = true;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.leader }).length <= 0) canSave = false;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.tank }).length <= 0) canSave = false;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.melee }).length <= 0) canSave = false;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.ranged }).length <= 0) canSave = false;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.flying }).length <= 0) canSave = false;
            if (getUserInfo(obj).currentUserInventory.filter(function (loot) { return loot._lootID === obj.building }).length <= 0) canSave = false;

            var sendJson = {};
            if (canSave)
            {
                exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Saving armory to playerID: " + getUserInfo(obj).currentUserObj.id);
                var updateArmoryQueryString = String.format("UPDATE armory SET troopLeader={0},troopTank={1},troopMelee={2},troopRanged={3},troopFlying={4},troopBuilding={5} WHERE playerID='{6}'", obj.leader, obj.tank, obj.melee, obj.ranged, obj.flying, obj.building, getUserInfo(obj).currentUserObj.id);
                queryFromDB(updateArmoryQueryString, function (result) {
                    if (result.affectedRows > 0) {
                        sendJson = { message: "ok" }
                    } else {
                        sendJson = { message: "Error on armory saving" }
                    }
                    io.to(obj.clientID).emit("DefaultCallback", sendJson);
                })
            } else {
                sendJson = { message: "Some troops not on your inventory" }
                io.to(obj.clientID).emit("DefaultCallback", sendJson);
            }
        })

        /////////////
        ////MATCH////
        /////////////

        var sendMatch = function (matchObj, obj)
        {
            var currOpponentID = "";
            if (matchObj.player1ID != getUserInfo(obj).currentUserObj.id)
                currOpponentID = matchObj.player1ID;
            else
                currOpponentID = matchObj.player2ID;
            if (currOpponentID != "" && currOpponentID != null)
            {
                var getMatchesQueryString = String.format("SELECT name FROM `users` WHERE id={0}", currOpponentID);
                queryFromDB(getMatchesQueryString, function (result) {
                    matchObj.opponentName = result[0].name;
                    io.to(obj.clientID).emit("UpdateClientMatches", matchObj);
                })
            } else {
                io.to(obj.clientID).emit("UpdateClientMatches", matchObj);
            }
        }

        socket.on('requestAllMatches', function (obj) {
            var getMatchesQueryString = String.format("SELECT * FROM `matches` WHERE player1ID={0} OR player2ID={0}", getUserInfo(obj).currentUserObj.id);
            queryFromDB(getMatchesQueryString, function (result) {
                exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending matches for player: " + getUserInfo(obj).currentUserObj.id);
                for (var i = 0; i < result.length; i++) {
                    sendMatch(result[i], obj);
                }
            })
        })
        socket.on('requestMatch', function (obj) {
            //Verifying if match  available for player
            for (var i = 0; i < getUserInfo(obj).currentUserInventory.length; i++) {
                if (getUserInfo(obj).currentUserInventory[i]._lootType == Utils.LOOT_TYPES.MATCHES.value)
                {
                    if (getUserInfo(obj).currentUserInventory[i]._lootAmount <= 0)
                    {
                        exports.userLogger.error(getUserInfo(obj).currentUserObj.id, "There isn't any match available for this player to use");
                        return;
                    }else{
                        exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Updating match count on player inventory");
                        addLootToInventory(obj, 1, "PlayerMatches", 1, -1);
                    }
                }
            }

            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Creating match for id: " + getUserInfo(obj).currentUserObj.id);
            var getMatchQueryString = String.format("SELECT id FROM `matches` WHERE player2ID IS NULL AND player1ID <> {0} AND status={1}", getUserInfo(obj).currentUserObj.id, Utils.MATCH_STATES.WAITING.value);
            queryFromDB(getMatchQueryString, function (result) {
                if (result.length > 0) {
                    exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Setting this player to match id " + result[0].id);
                    var updateMatchQueryString = String.format("UPDATE matches SET player2ID={0},status={1} WHERE id={2}", getUserInfo(obj).currentUserObj.id, Utils.MATCH_STATES.PLAYING.value, result[0].id);
                    queryFromDB(updateMatchQueryString, function (updateResult) {
                        var getUpdatedMatchQueryString = String.format("SELECT * FROM `matches` WHERE id={0}", result[0].id);
                        queryFromDB(getUpdatedMatchQueryString, function (result) {
                            if (result.length > 0) {
                                io.to(obj.clientID).emit("DefaultCallback", result[0]);
                            }
                        });
                    })
                } else {
                    exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Inserting new match for player id " + getUserInfo(obj).currentUserObj.id);
                    var insertMatchQueryString = String.format("INSERT INTO matches (player1ID,mapID,status) VALUES ('{0}','{1}','{2}')", getUserInfo(obj).currentUserObj.id, obj.mapID, Utils.MATCH_STATES.EMPTY.value);
                    queryFromDB(insertMatchQueryString, function (result) {
                        var getMatchByIDQueryString = String.format("SELECT * FROM `matches` WHERE id={0}", result.insertId);
                        queryFromDB(getMatchByIDQueryString, function (result) {
                            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending new match for player: " + getUserInfo(obj).currentUserObj.id);
                            io.to(obj.clientID).emit("DefaultCallback", result[0]);
                        })
                    })
                }
            })
        });

        ///////////////////
        ////AFTER MATCH////
        ///////////////////

        exports.afterMatch = function (_winnerID, _loserID, obj)
        {
            exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Updating aftermatch info");
            if (_winnerID != 0 && _winnerID != -1)
            {
                //Add loot box to winner
                var winnerInvQueryString = String.format("SELECT * FROM inventory WHERE inventory.playerID = {0}", _winnerID);
                queryFromDB(winnerInvQueryString, function (result) {
                    var userLootBox;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].lootType == Utils.LOOT_TYPES.BOX.value)
                            userLootBox = result[i];
                    }
                    var updateLootBoxQueryString = String.format("UPDATE inventory SET lootAmount={0} WHERE id={1}", (userLootBox.lootAmount + 1), userLootBox.id);
                    queryFromDB(updateLootBoxQueryString, function (updateResult) {
                        exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Loot box updated");
                        io.to(obj.clientID).emit("UpdateClientGameOver", { winnerID: _winnerID });

                        updatePlayerRanks(_winnerID, true, obj);
                        updatePlayerRanks(_loserID, false, obj);
                    })
                })
            } else {
                exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Match updated, sending game over to client");
                var _isWinner = _winnerID == getUserInfo(obj).id ? true : false;
                io.to(obj.clientID).emit("UpdateClientGameOver", { isWinner: -1, globalRank: getUserInfo(obj).globalRankExp, honorRank: getUserInfo(obj).honorRankExp, winStreak: getUserInfo(obj).winStreak });
            }

            exports.sendPushToUserID(_loserID, "You lost a match!", "Entre nas suas partidas completadas para ver seus resultados");
            exports.sendPushToUserID(_winnerID, "You won a match!", "Entre nas suas partidas completadas para ver seus resultados");
        }

        var updatePlayerRanks = function(userID, _isWinner, obj)
        {
            var getUserInfoQueryString = String.format("SELECT * FROM users WHERE id={0}", userID);
            queryFromDB(getUserInfoQueryString, function (userResult) {
                var userGlobalRank = userResult[0].globalRankExp;
                var userHonorRank = userResult[0].honorRankExp;
                var userWinStreak = userResult[0].winStreak > 0 ? 1 : userResult[0].winStreak;
                if (_isWinner)
                {
                    userGlobalRank += (userWinStreak > 0 ? (10 * userWinStreak) : 10);
                    userHonorRank += 20;
                    userWinStreak++;
                } else {
                    userGlobalRank -= 20;
                    userGlobalRank < 0 ? userGlobalRank = 0 : userGlobalRank = userGlobalRank;
                    userWinStreak = 0;
                }

                var updateUserRanksQueryString = String.format("UPDATE users SET globalRankExp={0},honorRankExp={1},winStreak={2} WHERE id={3}", userGlobalRank, userHonorRank, userWinStreak, userID);
                queryFromDB(updateUserRanksQueryString, function (updateResult) {
                    exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Player " + userID + " updated");

                    if (userID == getUserInfo(obj).currentUserObj.id)
                    {
                        exports.userLogger.info(getUserInfo(obj).currentUserObj.id, "Sending game over to client");
                        io.to(obj.clientID).emit("UpdateClientGameOver", { isWinner: _isWinner, globalRank: userGlobalRank, honorRank: userHonorRank, winStreak: userWinStreak });
                    }
                })
            })
        }

        ////////////
        ////PUSH////
        ////////////
        exports.sendPushToUserID = function(userID, pushTitle, pushMessage)
        {
            var userQueryString = String.format("SELECT * FROM users WHERE id={0}", userID);
            queryFromDB(userQueryString, function (result) {
                if (result.length > 0 && result[0].registrationID != null)
                {
                    log4js.getLogger('general').info("Sending push to user id " + userID);
                    GCM.sendPush(result[0].registrationID, pushTitle, pushMessage);
                    log4js.getLogger('general').info("Sending push to " + result[0].registrationID);
                }
            })
        }
    })
}