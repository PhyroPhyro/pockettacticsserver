exports.LootData = function (id, lootName, lootType, lootAmount, lootID, playerID, timeUpdated) {
    this._id = id;
    this._lootName = lootName;
    this._lootType = lootType;
    this._lootAmount = lootAmount;
    this._lootID = lootID;
    this._playerID = playerID;
    this._timeUpdated = timeUpdated;
}

exports.CellData = function (id, value, xCoord, yCoord, cellTroopUID) {
    this._id = id;
    this._value = value;
    this._xCoord = xCoord;
    this._yCoord = yCoord;
    this._cellTroopUID = cellTroopUID;
}

exports.NeighbourData = function (id, neighbours) {
    this._id = id;
    this._neighbours = neighbours;
}

exports.TroopData = function (playerID, uid, id, name, troopClass, life, cost, speed, atkDistance, atkStr, spawnableID, turnsToSpawn, typeID, status, troopAbility, canFly, canMove, canAttack, canAttackFlying, summonDistance) {
    this._id = id;
    this._uid = uid;
    this._playerID = playerID;
    this._name = name;
    this._life = life;
    this._maxLife = life;
    this._cost = cost;
    this._speed = speed;
    this._atkDistance = atkDistance;
    this._atkStr = atkStr;
    this._typeID = typeID;
    this._status = status;
    this._troopAbility = troopAbility;
    this._summonDistance = summonDistance;

    this._troopClass = troopClass;
    this._canFly = canFly;
    this._canMove = canMove;
    this._canAttack = canAttack;
    this._canAttackFlying = canAttackFlying;

    //Special Cases
    this._isProtected = 0;
    this._hasDoubleAtk = 0;
    this._turnChecked = 0;
    this._spawnableID = spawnableID;
    this._turnsToSpawn = turnsToSpawn;
    this._maxSpawnTurns = turnsToSpawn;
}

exports.TroopTypes = function (id, name, strongTo, weakTo) {
    this._id = id;
    this._name = name;
    this._strongTo = strongTo;
    this._weakTo = weakTo;
}

exports.TROOP_STATUS = {
    IDLE: { value: 0, name: "idle", code: "I" },
    MOVED: { value: 1, name: "moved", code: "M" },
    ATTACKED: { value: 2, name: "attacked", code: "A" }
};

exports.TROOP_CLASS = {
    LEADER: { value: 1, name: "leader", code: "L" },
    TANK: { value: 2, name: "tank", code: "T" },
    MELEE: { value: 3, name: "melee", code: "M" },
    RANGED: { value: 4, name: "ranged", code: "R" },
    FLYING: { value: 5, name: "flying", code: "F" },
    BUILDING: { value: 6, name: "building", code: "B" }
};

exports.TROOP_ABILITIES = {
    HASTE: { value: 1, name: "haste", code: "H" },
    BLOCK: { value: 2, name: "block", code: "B" },
    DOUBLE_STRIKE: { value: 3, name: "double", code: "D" },
    SPAWNER: { value: 4, name: "spawner", code: "S" },
	DEATH_LEECHER: { value: 5, name: "leecher", code: "DL" },
	DEATH_RISEN: { value: 6, name: "risen", code: "DR" },
	BURNING_SKIN: { value: 7, name: "burning", code: "BS" }
};

exports.CELL_VALUES = {
    EMPTY: { value: -1, name: "empty", code: "E" },
    NORMAL: { value: 0, name: "normal", code: "N" },
    HERO: { value: 1, name: "hero", code: "H" }
};

exports.MATCH_STATES = {
    EMPTY: { value: 0, name: "empty", code: "E" },
    WAITING: { value: 1, name: "waiting", code: "W" },
    PLAYING: { value: 2, name: "playing", code: "P" },
    COMPLETED: { value: 3, name: "completed", code: "C" },
};

exports.LOOT_TYPES = {
    MATCHES: { value: 1, name: "matches", code: "M" },
    TROOP: { value: 2, name: "troop", code: "T" },
    BOX: { value: 3, name: "box", code: "B" },
    CURRENCY: { value: 4, name: "currency", code: "C" },
    UPGRADE: { value: 5, name: "upgrade", code: "U" }
};

exports.HONOR_RANK = {
    CABLE: { value: 0, name: "Cable", code: "C" },
    GENERAL: { value: 500, name: "General", code: "G" },
    COMMANDER: { value: 1500, name: "Commander", code: "C" }
};

exports.GLOBAL_RANK = {
    BRONZE: { value: 0, name: "Bronze", code: "B" },
    SILVER: { value: 500, name: "Silver", code: "S" },
    GOLD: { value: 1500, name: "Gold", code: "G" }
};

exports.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

exports.isOdd = function(num) { return (num % 2) == 1; }