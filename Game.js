if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
            ? args[number]
            : match
            ;
        });
    };
}

var fs = require('fs')
var Database = require('./Database');
var Utils = require('./Utils');
var store = require('json-fs-store')();

exports = module.exports = function (io) {
    io.sockets.on('connection', function (socket) {

        /////////VARS
        var maxEachTroop = 5;

        ///////////METHODS
        var NewTroop = function (troopData)
        {
            var troop = new Utils.TroopData(0, 0, troopData._id, troopData._name,
                                      troopData._troopClass, troopData._life, troopData._cost,
                                      troopData._speed, troopData._atkDistance, troopData._atkStr, troopData._spawnableID, troopData._turnsToSpawn,
                                      troopData._typeID, Utils.TROOP_STATUS.ATTACKED.value, troopData._troopAbility, troopData._canFly,
                                      troopData._canMove, troopData._canAttack, troopData._canAttackFlying, troopData._summonDistance);

            return troop;
        }

        var NewTroopFromDB = function (troopDataDB, playerID, player)
        {
            var troop = new Utils.TroopData(playerID, 0, troopDataDB.id, troopDataDB.name,
                                      troopDataDB.troopClass, troopDataDB.life, troopDataDB.troopCost,
                                      troopDataDB.speed, troopDataDB.atkDistance, troopDataDB.atkStr, troopDataDB.spawnableID, troopDataDB.turnsToSpawn,
                                      troopDataDB.typeID, Utils.TROOP_STATUS.ATTACKED.value, troopDataDB.troopAbility, troopDataDB.canFly,
                                      troopDataDB.canMove, troopDataDB.canAtk, troopDataDB.canAtkFlying, troopDataDB.summonDistance);

            return troop;
        }

        var AddTroopToArmory = function (troopDataDB, playerID, player, obj) {
            var troop = new NewTroopFromDB(troopDataDB, playerID, player);
            if(player == 1)
                Database.getUserInfo(obj).currentArmoryTroops1.push(troop);
            else
                Database.getUserInfo(obj).currentArmoryTroops2.push(troop);
        }

        var AddTroopToMap = function (troopDataDB, playerID, cellID, matchPlayer, obj) {

            var newUID = getNewUID(obj);

            var troop = new Utils.TroopData(playerID, newUID, troopDataDB.id, troopDataDB.name,
                                      troopDataDB.troopClass, troopDataDB.life, troopDataDB.troopCost,
                                      troopDataDB.speed, troopDataDB.atkDistance, troopDataDB.atkStr, troopDataDB.spawnableID, troopDataDB.turnsToSpawn,
                                      troopDataDB.typeID, Utils.TROOP_STATUS.IDLE.value, troopDataDB.troopAbility, troopDataDB.canFly,
                                      troopDataDB.canMove, troopDataDB.canAtk, troopDataDB.canAtkFlying, troopDataDB.summonDistance);

            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Adding to map troop: " + troop._uid + " on cellID " + cellID);
            for (i in Database.getUserInfo(obj).currentMapCells) {
                if (Database.getUserInfo(obj).currentMapCells[i]._id == cellID) {
                    Database.getUserInfo(obj).currentMapCells[i]._cellTroopUID = troop._uid;
                    updateClientCell(cellID, obj);
                }
            }

            if (troop._troopClass == Utils.TROOP_CLASS.LEADER.value)
            {
                if(matchPlayer == 1)
                    Database.getUserInfo(obj).currentHero1UID = newUID;
                else
                    Database.getUserInfo(obj).currentHero2UID = newUID;
            }

            Database.getUserInfo(obj).currentMapTroops.push(troop);
            updateClientTroop(troop._uid, obj);
        }

        function updateClientMap(obj) {
            for (i in Database.getUserInfo(obj).currentMapCells) {
                io.to(obj.clientID).emit("UpdateClientMap", Database.getUserInfo(obj).currentMapCells[i]);
            }
            var sendMessage = {
                message: "done"
            }
            io.to(obj.clientID).emit("UpdateClientMap", sendMessage);
        }

        function updateClientCell(cellID, obj) {
            for (i in Database.getUserInfo(obj).currentMapCells) {
                if (Database.getUserInfo(obj).currentMapCells[i]._id == cellID) {
                    Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Updating cell " + cellID + " on client");
                    io.to(obj.clientID).emit("UpdateClientMap", Database.getUserInfo(obj).currentMapCells[i]);
                }
            }
        }

        function updateClientTroops(obj) {
            for (i in Database.getUserInfo(obj).currentMapTroops) {
                io.to(obj.clientID).emit("UpdateClientTroops", Database.getUserInfo(obj).currentMapTroops[i]);
            }
            var sendMessage = {
                message: "done"
            }
            io.to(obj.clientID).emit("UpdateClientTroops", sendMessage);
        }

        function updateClientTroop(troopUID, obj) {
            for (i in Database.getUserInfo(obj).currentMapTroops) {
                if (Database.getUserInfo(obj).currentMapTroops[i]._uid == troopUID) {
                    Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Sending troop of index " + i);
                    Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Updating troop UID " + Database.getUserInfo(obj).currentMapTroops[i]._uid);
                    io.to(obj.clientID).emit("UpdateClientTroops", Database.getUserInfo(obj).currentMapTroops[i]);
                }
            }
            var sendMessage = {
                message: "done"
            }
            io.to(obj.clientID).emit("UpdateClientTroops", sendMessage);
        }

        function updateClientArmory(obj) {
            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Updating player client armory")
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            var currentArmory;
            if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
                currentArmory = Database.getUserInfo(obj).currentArmoryTroops1;
            else
                currentArmory = Database.getUserInfo(obj).currentArmoryTroops2;

            for (i in currentArmory) {
                io.to(obj.clientID).emit("UpdateClientArmory", currentArmory[i]);
            }
        }

        function updateClientEnergy(obj) {
            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Updating player client energy")
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            var energy;
            if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
                energy = Database.getUserInfo(obj).currentEnergy1;
            else
                energy = Database.getUserInfo(obj).currentEnergy2;

            var sendJson = { playerEnergy: energy }
            io.to(obj.clientID).emit("UpdateClientEnergy", sendJson);
        }

        function loadTurnByID(turnID, obj) {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            store.load(turnID.toString(), function (err, object) {
                if (err) throw err; // err if JSON parsing failed 
                var sendJson;

                //Check if current player is meant to be playing now
                if (object._lastPlayedUser == currentUserObj.id)
                {
                    sendJson = {
                        message: "It's not your turn on this match."
                    }
                    io.to(obj.clientID).emit("DefaultCallback", sendJson);
                    return;
                }

                Database.getUserInfo(obj).currentTurnSaveState = object;

                Database.getUserInfo(obj).currentEnergy1 = object._currentEnergy1;
                Database.getUserInfo(obj).currentEnergy2 = object._currentEnergy2;

                sendJson = {
                    message: "done"
                }

                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Turn Loaded");

                io.to(obj.clientID).emit("DefaultCallback", sendJson);

                updateClientEnergy(obj);
                return object;
            });
        }

        function saveTurnByID(turnID, hasTurnEnded, obj) {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            var lastPlayer;
            if (hasTurnEnded)
                lastPlayer = currentUserObj.id;
            else if (Database.getUserInfo(obj).currentTurnSaveState == null)
                lastPlayer = 0;
            else
                lastPlayer = Database.getUserInfo(obj).currentTurnSaveState._lastPlayedUser;

            if (turnID != 0)
            {
                var storeObj = {
                    id: turnID.toString(),
                    name: turnID,
                    _currentMapCells: Database.getUserInfo(obj).currentMapCells,
                    _currentMapNeighbourCells: Database.getUserInfo(obj).currentMapNeighbourCells,
                    _currentMapTroops: Database.getUserInfo(obj).currentMapTroops,
                    _currentEnergy1: Database.getUserInfo(obj).currentEnergy1,
                    _currentEnergy2: Database.getUserInfo(obj).currentEnergy2,
                    _currentArmoryTroops1: Database.getUserInfo(obj).currentArmoryTroops1,
                    _currentArmoryTroops2: Database.getUserInfo(obj).currentArmoryTroops2,
                    _currentHero1UID: Database.getUserInfo(obj).currentHero1UID,
                    _currentHero2UID: Database.getUserInfo(obj).currentHero2UID,
                    _lastPlayedUser: lastPlayer
                };

                store.add(storeObj, function (err, object) {
                    if (err) throw err; // err if JSON parsing failed 

                    return object;
                });
            }
        }

        function getMatchTroopByUID(troopUID, obj) {
            for (i in Database.getUserInfo(obj).currentMapTroops) {
                if (Database.getUserInfo(obj).currentMapTroops[i]._uid == troopUID)
                    return Database.getUserInfo(obj).currentMapTroops[i];
            }
            return null;
        }   

        function removeMatchTroopByUID(troopUID, obj) {
            for (i in Database.getUserInfo(obj).currentMapTroops) {
                if (Database.getUserInfo(obj).currentMapTroops[i]._uid == troopUID)
                    Database.getUserInfo(obj).currentMapTroops.splice(i, 1);
            }
        }

        function getCellByTroopUID (troopUID, obj) {
            for (i in Database.getUserInfo(obj).currentMapCells) {
                if (Database.getUserInfo(obj).currentMapCells[i]._cellTroopUID == troopUID)
                    return Database.getUserInfo(obj).currentMapCells[i];
            }

            return null;
        }

        function getCellByID(ID, obj) {
            for (var i = 0; i < Database.getUserInfo(obj).currentMapCells.length; i++) {
                if (Database.getUserInfo(obj).currentMapCells[i]._id == ID)
                    return Database.getUserInfo(obj).currentMapCells[i];
            }
            return null;
        }

        function getCellByCoord(x, y, obj) {
            for (i in Database.getUserInfo(obj).currentMapCells) {
                if (Database.getUserInfo(obj).currentMapCells[i]._xCoord == x && Database.getUserInfo(obj).currentMapCells[i]._yCoord)
                    return Database.getUserInfo(obj).currentMapCells[i];
            }

            return null;
        }

        function getTypeByID(ID, obj) {
            for (i in Database.getUserInfo(obj).currentTroopTypes)
            {
                if (Database.getUserInfo(obj).currentTroopTypes[i]._id == ID)
                    return Database.getUserInfo(obj).currentTroopTypes[i];
            }

            return null;
        }

        function getTroopByUID(UID, obj) {
            for (i in Database.getUserInfo(obj).currentMapTroops)
            {
                if (Database.getUserInfo(obj).currentMapTroops[i]._uid == UID)
                    return Database.getUserInfo(obj).currentMapTroops[i];
            }

            return null;
        }

        function getNewUID(obj) {
            var greaterUID = 0;
            for (i in Database.getUserInfo(obj).currentMapTroops)
            {
                if (Database.getUserInfo(obj).currentMapTroops[i]._uid > greaterUID)
                    greaterUID = Database.getUserInfo(obj).currentMapTroops[i]._uid;
            }

            return greaterUID + 1;
        }

        function getTroopQuantity(troopID, obj) {
            var troopCounter = 0;
            for (var i = 0; i < Database.getUserInfo(obj).currentMapTroops.length; i++) {
                if (Database.getUserInfo(obj).currentMapTroops[i]._id == troopID)
                    troopCounter++;
            }

            return troopCounter;
        }

        var setupHeroByUserID = function (player, obj) {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Setting up player hero")
            var heroesQueryString = String.format("SELECT troopLeader FROM armory WHERE playerID={0}", currentUserObj.id);
            Database.queryFromDB(heroesQueryString, function (result) {
                if (result.length > 0) {
                    var heroCells = [];
                    for (var i = 0; i < Database.getUserInfo(obj).currentMapCells.length; i++) {
                        if (Database.getUserInfo(obj).currentMapCells[i]._value == Utils.CELL_VALUES.HERO.value)
                        {
                            heroCells.push(Database.getUserInfo(obj).currentMapCells[i]._id);
                        }
                    }
                    var heroTroopQueryString = String.format("SELECT * FROM troops WHERE id={0}", result[0].troopLeader);
                    Database.queryFromDB(heroTroopQueryString, function (resultTroop) {
                        AddTroopToMap(resultTroop[0], currentUserObj.id, heroCells[player - 1], player, obj);
                    })
                }

                setupArmoryByUserID(player, obj);
            })
        }

        var setupArmoryByUserID = function (player, obj) {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Setting up player armory for player " + currentUserObj.id);

            var queryString = String.format("SELECT * FROM troops WHERE troops.id = (SELECT troopTank FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopMelee FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopRanged FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopFlying FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1) OR troops.id = (SELECT troopBuilding FROM armory WHERE armory.playerID = {0} ORDER BY id DESC LIMIT 1)", currentUserObj.id);
            Database.queryFromDB(queryString, function (resultTroops) {
                if (resultTroops.length > 0) {
                    for (i in resultTroops) {
                        AddTroopToArmory(resultTroops[i], currentUserObj.id, player, obj);
                    }

                    updateClientArmory(obj);
                }
            })
        }

        var setupTroopTypes = function(obj)
        {
            var typesQueryString = "SELECT * FROM types";
            Database.queryFromDB(typesQueryString, function (result) {
                Database.getUserInfo(obj).currentTroopTypes = [];
                for (i in result)
                {
                    var newType = new Utils.TroopTypes(result[i].id, result[i].name, result[i].strongTo, result[i].weakTo);
                    Database.getUserInfo(obj).currentTroopTypes.push(newType);
                }
            })
        }

        var moveTroop = function (fromCellID, toCellID, troopUID, obj)
        {
            var fromCell = getCellByID(fromCellID, obj);
            var toCell = getCellByID(toCellID, obj);

            toCell._cellTroopUID = fromCell._cellTroopUID;
            fromCell._cellTroopUID = 0;

            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Moving troop " + troopUID + " from cell " + fromCellID + " to cell " + toCellID);

            updateClientCell(fromCellID, obj);
            updateClientCell(toCellID, obj);

            var currTroop = getMatchTroopByUID(troopUID, obj);
            currTroop._status = Utils.TROOP_STATUS.MOVED.value;
            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Updated troop " + troopUID + " status to MOVED");
            updateClientTroop(troopUID, obj);

            saveTurnByID(Database.getUserInfo(obj).currentTurnID, false, obj);
        }

        var attackTroop = function (fromCellID, toCellID, obj)
        {
            var currentAttackingTroop = getMatchTroopByUID(getCellByID(fromCellID, obj)._cellTroopUID, obj);
            var currentReceivingTroop = getMatchTroopByUID(getCellByID(toCellID, obj)._cellTroopUID, obj);

            if (currentReceivingTroop._isProtected)
            {
                currentReceivingTroop._isProtected = 0;
            } else {
                var damage = currentAttackingTroop._atkStr;

                if (getTypeByID(currentReceivingTroop._typeID, obj)._weakTo == currentAttackingTroop._typeID)
                    damage = damage * 1.5;
                else if (getTypeByID(currentReceivingTroop._typeID, obj)._strongTo == currentAttackingTroop._typeID)
                    damage = damage * 0.5;

                if (currentAttackingTroop._hasDoubleAtk == 1)
                    damage = damage * 2;

                currentReceivingTroop._life -= Math.round(damage);
            }

            updateClientTroop(currentReceivingTroop._uid, obj);

            currentAttackingTroop._status = Utils.TROOP_STATUS.ATTACKED.value;
            currentAttackingTroop._hasDoubleAtk = 0;

            updateClientTroop(currentAttackingTroop._uid, obj);

            saveTurnByID(Database.getUserInfo(obj).currentTurnID, false, obj);
        }

        var summonTroop = function (troopData, cellID, obj)
        {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            if (getTroopQuantity(troopData._id, obj) < maxEachTroop)
            {
                var currUID = getNewUID(obj);
                troopData._uid = currUID;
                troopData._playerID = currentUserObj.id;

                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Adding to map troop: " + troopData._uid + " on cellID " + cellID);
                for (i in Database.getUserInfo(obj).currentMapCells) {
                    if (Database.getUserInfo(obj).currentMapCells[i]._id == cellID) {
                        Database.getUserInfo(obj).currentMapCells[i]._cellTroopUID = troopData._uid;
                        updateClientCell(cellID, obj);
                    }
                }

                Database.getUserInfo(obj).currentMapTroops.push(troopData);
                specialSummon(troopData, obj);

                updateClientTroop(troopData._uid, obj);

                if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
                    Database.getUserInfo(obj).currentEnergy1 -= troopData._cost;
                else
                    Database.getUserInfo(obj).currentEnergy2 -= troopData._cost;

                updateClientEnergy(obj);

                saveTurnByID(Database.getUserInfo(obj).currentTurnID, false, obj);
            } else {
                var sendMessage = {
                    message: "Troop achieved max quantity on field."
                }
                io.to(obj.clientID).emit("UpdateClientTroops", sendMessage);
            }
        }

        //Special Cases
        var specialSummon = function (troopData, obj)
        {
            if (troopData._troopAbility == Utils.TROOP_ABILITIES.HASTE.value)
                troopData._status = Utils.TROOP_STATUS.IDLE.value;

            if (troopData._troopAbility == Utils.TROOP_ABILITIES.BLOCK.value)
                troopData._isProtected = 1;

            if (troopData._troopAbility == Utils.TROOP_ABILITIES.DOUBLE_STRIKE.value)
                troopData._hasDoubleAtk = 1;

            if (troopData._troopAbility == Utils.TROOP_ABILITIES.SPAWNER.value)
                spawnTroopNear(troopData._uid, obj);
        }

        var specialTroopsTurn = function (obj)
        {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            for (var i = 0; i < Database.getUserInfo(obj).currentMapTroops.length; i++) {
                var currTroop = Database.getUserInfo(obj).currentMapTroops[i];
                if (currTroop._playerID == currentUserObj.id && currTroop._turnChecked != Database.getUserInfo(obj).currentTurnID && currTroop._troopClass == Utils.TROOP_CLASS.BUILDING.value && currTroop._spawnableID != 0)
                {
                    currTroop._turnChecked = Database.getUserInfo(obj).currentTurnID;
                    currTroop._turnsToSpawn --;
                    if (currTroop._turnsToSpawn <= 0)
                    {
                        spawnTroopNear(currTroop._uid);
                        currTroop._turnsToSpawn = currTroop._maxSpawnTurns;
                    }
                    updateClientTroop(currTroop._uid, obj);
                }
            }
        }

        var spawnTroopNear = function (spawnerUID, obj)
        {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;

            var troopCell = getCellByTroopUID(spawnerUID, obj);
            var cellNeighbours = Database.getUserInfo(obj).currentMapNeighbourCells[(troopCell._id - 1)]._neighbours;
            var canSpawn = false;
            for (var i = 0; i < cellNeighbours.length; i++) {
                if (getCellByID(cellNeighbours[i]._id, obj)._cellTroopUID == 0)
                    canSpawn = true;
            }
            if (!canSpawn)
            {
                Database.userLogger.error(Database.getUserInfo(obj).currentUserObj.id, "Can't spawn near, no available cells");
                return;
            }

            var rng = Utils.getRandomInt(0, cellNeighbours.length - 1);
            var chosenCell = getCellByID(cellNeighbours[rng]._id, obj);
            
            while (chosenCell._cellTroopUID != 0)
            {
                rng = Utils.getRandomInt(0, cellNeighbours.length - 1);
                chosenCell = getCellByID(cellNeighbours[rng]._id, obj);
            }

            var getSpawnableQueryString = String.format("SELECT * FROM troops WHERE id={0}", getTroopByUID(spawnerUID, obj)._spawnableID);
            Database.queryFromDB(getSpawnableQueryString, function (result) {
                var currPlayer = 0;
                if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
                    currPlayer = 1;
                else
                    currPlayer = 2;

                var troop = new NewTroopFromDB(result[0], currentUserObj.id, currPlayer);
                summonTroop(troop, chosenCell._id, obj);
            })
        }

        var checkTroops = function (obj)
        {
            var winnerID = 0;

            for (var i = Database.getUserInfo(obj).currentMapTroops.length - 1; i >= 0; i--)
            {
                if (Database.getUserInfo(obj).currentMapTroops[i]._life <= 0) {
                    Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Troop UID " + Database.getUserInfo(obj).currentMapTroops[i]._uid + " has died, being removed");

                    //Check Victory
                    if (Database.getUserInfo(obj).currentMapTroops[i]._troopClass == Utils.TROOP_CLASS.LEADER.value)
                    {
                        if (winnerID == 0)
                        {
                            if (Database.getUserInfo(obj).currentMatchPlayer1ID != Database.getUserInfo(obj).currentMapTroops[i]._playerID)
                                winnerID = Database.getUserInfo(obj).currentMatchPlayer1ID;
                            else
                                finishGame(Database.getUserInfo(obj).currentMatchPlayer2ID, obj);
                        } else {
                            winnerID = -1;
                        }
                    }

                    var troopCell = getCellByTroopUID(Database.getUserInfo(obj).currentMapTroops[i]._uid, obj);
                    troopCell._cellTroopUID = 0;
                    updateClientCell(troopCell._id, obj);
                    Database.getUserInfo(obj).currentMapTroops.splice(i, 1);
                }
            }

            if (winnerID != 0)
                finishGame(winnerID, obj);
        }

        var finishGame = function(_winnerID, obj)
        {
            Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Game " + Database.getUserInfo(obj).currentMatchID + " finished, updating match status");
            var updateMatchQueryString = String.format("UPDATE matches SET status={0},lastPlayerID={1},winnerID={2} WHERE id={3}", Utils.MATCH_STATES.COMPLETED.value, Database.getUserInfo(obj).currentUserObj.id, _winnerID, Database.getUserInfo(obj).currentMatchID);
            Database.queryFromDB(updateMatchQueryString, function (updateResult) {
                if (_winnerID == Database.getUserInfo(obj).currentMatchPlayer1ID)
                    Database.afterMatch(_winnerID, Database.getUserInfo(obj).currentMatchPlayer2ID, obj);
                else
                    Database.afterMatch(_winnerID, Database.getUserInfo(obj).currentMatchPlayer1ID, obj)
            })
        }

        /////////ONS
        socket.on('gameSetup', function (obj) {

            //Empty everything before setting
            Database.getUserInfo(obj).currentMapCells = [];
            Database.getUserInfo(obj).currentMapTroops = [];
            Database.getUserInfo(obj).currentEnergy1 = 0;
            Database.getUserInfo(obj).currentEnergy2 = 0;
            Database.getUserInfo(obj).currentArmoryTroops1 = [];
            Database.getUserInfo(obj).currentArmoryTroops2 = [];
            Database.getUserInfo(obj).currentHero1UID = 0;
            Database.getUserInfo(obj).currentHero2UID = 0;
            Database.getUserInfo(obj).currentMatchID = obj.matchID;

            Database.getUserByToken(obj.token, function (user) {
                Database.getUserInfo(obj).currentUserObj = user;

                setupTroopTypes(obj);

                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Setting up match id " + Database.getUserInfo(obj).currentMatchID);

                var matchQueryString = String.format("SELECT * FROM matches WHERE id='{0}'", Database.getUserInfo(obj).currentMatchID);
                Database.queryFromDB(matchQueryString, function (result) {
                    var sendJson;
                    if (result.length > 0) {
                        Database.getUserInfo(obj).currentTurnID = result[0].currentTurnID;
                        Database.getUserInfo(obj).currentMatchPlayer1ID = result[0].player1ID;
                        Database.getUserInfo(obj).currentMatchPlayer2ID = result[0].player2ID;
                        Database.getUserInfo(obj).currentMapID = result[0].mapID;
                        if (Database.getUserInfo(obj).currentTurnID != 0)
                        {
                            loadTurnByID(Database.getUserInfo(obj).currentTurnID, obj);
                        } else {
                            Database.getUserInfo(obj).currentEnergy1 = 10;
                            Database.getUserInfo(obj).currentEnergy2 = 10;
                            updateClientEnergy(obj);
                            sendJson = {
                                message: "done"
                            }
                            io.to(obj.clientID).emit("DefaultCallback", sendJson);
                        }
                    } else {
                        Database.userLogger.error(Database.getUserInfo(obj).currentUserObj.id, "Error in match setup");

                        sendJson = {
                            message: "error"
                        }
                        io.to(obj.clientID).emit("DefaultCallback", sendJson);
                    }

                })
            })
        })

        socket.on('requestMap', function (obj) {
            if (Database.getUserInfo(obj).currentTurnID == 0) {
                //Setup fresh map
                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Generating new map: " + Database.getUserInfo(obj).currentMapID);
                var currentMap = [];
                Database.getUserInfo(obj).currentMapCells = [];
                var linesArray = fs.readFileSync('maps/map' + Database.getUserInfo(obj).currentMapID + '.csv').toString().split("\n");
                for (i in linesArray) {
                    linesArray[i] = linesArray[i].replace("\r", "");
                    currentMap.push(linesArray[i].split(","));
                }
                currentMap.pop();

                var cellCount = 0;
                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Creating new cells for mapID: " + Database.getUserInfo(obj).currentMapID);
                for (i in currentMap) {
                    for (j in currentMap[i]) {
                        cellCount++;
                        var currentCellData = new Utils.CellData(cellCount, currentMap[i][j], j, i, 0);
                        Database.getUserInfo(obj).currentMapCells.push(currentCellData);
                    }
                }

                //Set cell server neighburs
                Database.getUserInfo(obj).currentMapNeighbourCells = [];
                var currentMapCells = Database.getUserInfo(obj).currentMapCells;
                for (i in Database.getUserInfo(obj).currentMapCells)
                {
                    var thisCellX = parseInt(currentMapCells[i]._xCoord);
                    var thisCellY = parseInt(currentMapCells[i]._yCoord);
                    var neighbourArray = [];
                    for (var j = 0; j < currentMapCells.length; j++)
                    {

                        if (currentMapCells[j]._yCoord == thisCellY &&
                            (currentMapCells[j]._xCoord == (thisCellX + 1) ||
                            currentMapCells[j]._xCoord == (thisCellX - 1)))
                            neighbourArray.push(currentMapCells[j]);

                        if (currentMapCells[j]._xCoord == thisCellX &&
                            (currentMapCells[j]._yCoord == (thisCellY - 1) ||
                            currentMapCells[j]._yCoord == (thisCellY + 1)))
                            neighbourArray.push(currentMapCells[j]);

                        if (Utils.isOdd(currentMapCells[i]._yCoord))
                        {
                            if ((currentMapCells[j]._yCoord == (thisCellY + 1) || currentMapCells[j]._yCoord == (thisCellY - 1)) &&
                                currentMapCells[j]._xCoord == (thisCellX + 1))
                                neighbourArray.push(currentMapCells[j]);
                        } else {
                            if ((currentMapCells[j]._yCoord == (thisCellY + 1) || currentMapCells[j]._yCoord == (thisCellY - 1)) &&
                                currentMapCells[j]._xCoord == (thisCellX - 1))
                                neighbourArray.push(currentMapCells[j]);
                        }
                    }
                    Database.getUserInfo(obj).currentMapNeighbourCells.push(new Utils.NeighbourData(currentMapCells[i]._id, neighbourArray));
                }

                Database.userLogger.trace(Database.getUserInfo(obj).currentUserObj.id, "Done creating new cells for mapID: " + Database.getUserInfo(obj).currentMapID);
            } else {
                Database.getUserInfo(obj).currentMapCells = Database.getUserInfo(obj).currentTurnSaveState._currentMapCells;
                Database.getUserInfo(obj).currentMapNeighbourCells = Database.getUserInfo(obj).currentTurnSaveState._currentMapNeighbourCells;
            }
            updateClientMap(obj);
        });

        socket.on('requestTroops', function (obj) {
            Database.getUserInfo(obj).currentMapTroops = [];
            if (Database.getUserInfo(obj).currentTurnID == 0) {
                //Setup fresh hero
                setupHeroByUserID(1, obj);
            } else {
                Database.getUserInfo(obj).currentMapTroops = Database.getUserInfo(obj).currentTurnSaveState._currentMapTroops;
                Database.getUserInfo(obj).currentArmoryTroops1 = Database.getUserInfo(obj).currentTurnSaveState._currentArmoryTroops1;
                Database.getUserInfo(obj).currentArmoryTroops2 = Database.getUserInfo(obj).currentTurnSaveState._currentArmoryTroops2;

                for (i in Database.getUserInfo(obj).currentMapTroops) {
                    if (Database.getUserInfo(obj).currentMapTroops[i]._troopClass == Utils.TROOP_CLASS.LEADER.value) {
                        if (Database.getUserInfo(obj).currentMapTroops[i]._playerID == Database.getUserInfo(obj).currentMatchPlayer1ID)
                            Database.getUserInfo(obj).currentHero1UID = Database.getUserInfo(obj).currentMapTroops[i]._uid;
                        else
                            Database.getUserInfo(obj).currentHero2UID = Database.getUserInfo(obj).currentMapTroops[i]._uid;
                    }
                }
                if (Database.getUserInfo(obj).currentHero2UID == 0) {
                    setupHeroByUserID(2, obj);
                }

                updateClientTroops(obj);
                updateClientArmory(obj);
                specialTroopsTurn(obj);
            }
        });

        socket.on('requestEndTurn', function (obj) {
            var currentUserObj = Database.getUserInfo(obj).currentUserObj;
            var insertTurnQueryString = String.format("INSERT INTO turns (lastSavedPlayer,matchID) VALUES ('{0}','{1}')", currentUserObj.id, Database.getUserInfo(obj).currentMatchID);
            Database.queryFromDB(insertTurnQueryString, function (result) {
                if (result != null) {
                    for (i in Database.getUserInfo(obj).currentMapTroops) {
                        Database.getUserInfo(obj).currentMapTroops[i]._status = Utils.TROOP_STATUS.IDLE.value;
                    }

                    var opponentID;

                    if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
                    {
                        opponentID = Database.getUserInfo(obj).currentMatchPlayer2ID;
                        Database.getUserInfo(obj).currentEnergy1 += 2;
                    }
                    else
                    {
                        opponentID = Database.getUserInfo(obj).currentMatchPlayer1ID;
                        Database.getUserInfo(obj).currentEnergy2 += 2;
                    }

                    saveTurnByID(result.insertId, true, obj);
                    var matchStatus;

                    if (Database.getUserInfo(obj).currentMatchPlayer2ID == null)
                        matchStatus = Utils.MATCH_STATES.WAITING.value;
                    else
                        matchStatus = Utils.MATCH_STATES.PLAYING.value;

                    var updateMatchQueryString = String.format("UPDATE matches SET currentTurnID={0},status={1},lastPlayerID={2} WHERE id='{3}'", result.insertId, matchStatus, currentUserObj.id, Database.getUserInfo(obj).currentMatchID);
                    Database.queryFromDB(updateMatchQueryString, function (result) {
                        var sendJson = {
                            message: "done"
                        }

                        Database.sendPushToUserID(opponentID, "Sua vez!", String.format("Faça sua jogada contra {0}", currentUserObj.name));
                        io.to(obj.clientID).emit("DefaultCallback", sendJson);
                    });
                }
            });
        });

        ///////////VERIFICATIONS
        socket.on('verifyCellPath', function (obj) {
            var canMove = true;
            var errorMsg;

            var currentTroop = getMatchTroopByUID(obj.troopUID, obj);

            if (obj.fromCellID != getCellByTroopUID(obj.troopUID, obj)._id) {
                errorMsg = "Troop coming from invalid cell";
                canMove = false;
            }

            if (currentTroop._status != Utils.TROOP_STATUS.IDLE.value) {
                errorMsg = "Troop moving on wrong state: " + currentTroop._status;
                canMove = false;
            }

            var toCellID = 0;
            var cellCount = 0;
            while (obj["cell" + (cellCount + 1)] != null) {
                if (obj["cell" + (cellCount + 1)] != null)
                {
                    var cellID = obj["cell" + (cellCount + 1)];
                    var thisCell = getCellByID(cellID, obj);
                    var nextCell = getCellByID(obj["cell" + (cellCount + 1)], obj)
                    if (!nextCell in Database.getUserInfo(obj).currentMapNeighbourCells[cellID - 1]._neighbours)
                    {
                        errorMsg = "Troop passing through not neighbour cells";
                        canMove = false;
                    }
                }

                if (getCellByID(cellID, obj)._cellTroopUID != obj.troopUID && getCellByID(cellID, obj)._cellTroopUID != 0)
                {                    
                    errorMsg = "Troop passing through occupied cell";
                    canMove = false;
                }

                cellCount++;
                toCellID = cellID;

                if (getCellByID(toCellID, obj)._value == Utils.CELL_VALUES.EMPTY.value) {
                    errorMsg = "Troop passing through empty cell";
                    canMove = false;
                }
            }

            if (currentTroop._speed < cellCount - 1) {
                errorMsg = "Troop passing through invalid cell count";
                canMove = false;
            }

            var sendJson;
            if (canMove) {
                sendJson = { message: "ok" }

                moveTroop(obj.fromCellID, toCellID, obj.troopUID, obj);
            }
            else {
                sendJson = { message: errorMsg }
                Database.userLogger.warn(Database.getUserInfo(obj).currentUserObj.id, errorMsg);
            }

            io.to(obj.clientID).emit("DefaultCallback", sendJson);
        });

        socket.on('verifyAttackPath', function (obj) {
            var canAttack = true;
            var errorMsg;

            var currentAttackingTroop = getMatchTroopByUID(getCellByID(obj.attackFromID, obj)._cellTroopUID, obj);
            var currentDefendingTroop = getMatchTroopByUID(getCellByID(obj.attackToID, obj)._cellTroopUID, obj);

            if (currentAttackingTroop._status == Utils.TROOP_STATUS.ATTACKED.value) {
                errorMsg = "Troop attacking on wrong state: " + currentAttackingTroop._status;
                canAttack = false;
            }

            var distanceCellCount = 0;
            while (obj["distanceCell" + (distanceCellCount+1)] != null) {
                distanceCellCount++;
            }

            if (currentAttackingTroop._atkDistance < distanceCellCount - 1) {
                errorMsg = "Troop attacking from way to far";
                canAttack = false;
            }
            if (!currentAttackingTroop._canAttack && !currentDefendingTroop._canFly) {
                errorMsg = "Troop can't attack";
                canAttack = false;
            }
            if (!currentAttackingTroop._canAttackFlying && currentDefendingTroop._canFly) {
                errorMsg = "Troop can't attack flying troops";
                canAttack = false;
            }

            if (getCellByID(obj.attackToID, obj)._value == Utils.CELL_VALUES.EMPTY.value) {
                errorMsg = "Troop attacking empty cell";
                canAttack = false;
            }

            var sendJson;
            if (canAttack) {
                sendJson = { message: "ok" }

                attackTroop(obj.attackFromID, obj.attackToID, obj);
                attackTroop(obj.attackToID, obj.attackFromID, obj);
                checkTroops(obj);
            }
            else {
                sendJson = { message: errorMsg }
                Database.userLogger.warn(Database.getUserInfo(obj).currentUserObj.id, errorMsg);
            }

            io.to(obj.clientID).emit("DefaultCallback", sendJson);
        });

        socket.on('verifySummonPath', function (obj) {
            var canSummon = true;
            var errorMsg;

            var currentNewTroopID = obj.troopArmoryID;
            var summonCellID = obj.summonCellID;

            var currentPlayerHeroUID = 0;
            var currPlayerEnergy;
            var currentArmoryTroops = [];

            var currentUserObj = Database.getUserInfo(obj).currentUserObj;
            if (currentUserObj.id == Database.getUserInfo(obj).currentMatchPlayer1ID)
            {
                currentPlayerHeroUID = Database.getUserInfo(obj).currentHero1UID;
                currentArmoryTroops = Database.getUserInfo(obj).currentArmoryTroops1;
                currPlayerEnergy = Database.getUserInfo(obj).currentEnergy1;
            }
            else
            {
                currentPlayerHeroUID = Database.getUserInfo(obj).currentHero2UID;
                currentArmoryTroops = Database.getUserInfo(obj).currentArmoryTroops2;
                currPlayerEnergy = Database.getUserInfo(obj).currentEnergy2;
            }

            var summonCell = getCellByID(summonCellID, obj);
            var heroTroop = getMatchTroopByUID(currentPlayerHeroUID, obj);
            var heroCell = getCellByTroopUID(currentPlayerHeroUID, obj);
            var maxSummonDistance = heroTroop._summonDistance;

            if (summonCell._xCoord - heroCell._xCoord > maxSummonDistance ||
                summonCell._xCoord - heroCell._xCoord < -maxSummonDistance||
                summonCell._yCoord - heroCell._yCoord > maxSummonDistance ||
                summonCell._yCoord - heroCell._yCoord < -maxSummonDistance)
            {
                canSummon = false;
                errorMsg = "Trying to summon troop on invalid distance";
            }

            var newTroop = null;
            for (i in currentArmoryTroops)
            {
                if (currentNewTroopID == currentArmoryTroops[i]._id)
                    newTroop = currentArmoryTroops[i];
            }

            if (newTroop == null)
            {
                canSummon = false;
                errorMsg = "Trying to summon troop that is not in the player's armory";
            }else if(newTroop._cost  > currPlayerEnergy)
            {
                canSummon = false;
                errorMsg = "Trying to summon troop that has cost greater than current energy"
            }

            //Criar sistema de energia (setup, save, load das energias somente deste player)
            //Colocar condição de energia e custo aqui

            var sendJson;
            if (canSummon) {
                sendJson = { message: "ok" }

                summonTroop(NewTroop(newTroop), summonCellID, obj);
            }
            else {
                sendJson = { message: errorMsg }
                Database.userLogger.warn(Database.getUserInfo(obj).currentUserObj.id, errorMsg);
            }

            io.to(obj.clientID).emit("DefaultCallback", sendJson);
        });
    });
}
