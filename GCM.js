var gcm = require('node-gcm');

exports.sendPush = function(registrationID, messageTitle, messageBody)
{
    var message = new gcm.Message({
        collapseKey: 'demo',
        priority: 'high',
        contentAvailable: true,
        delayWhileIdle: true,
        timeToLive: 3,
        restrictedPackageName: "com.gametabula.pocketactics",
        data: {
            title: messageTitle,
            subtitle: "",
            alert: messageBody,
            json: {"big_picture_url": "",
                    "string_extra": "100000583627394",
                    "string_value": "value",
                    "string_key": "key",
                    "is_public": true,
                    "item_type_id": 4,
                    "numeric_extra": 0}
        }
    });

    var sender = new gcm.Sender('AIzaSyC_gW-wzOHLXaVfztyaWp62jVnCPZVfpM4');

    var registrationTokens = [];
    registrationTokens.push(registrationID);
    //registrationTokens.push('APA91bHPMwe6ofpN_1Qcjc0DSDccOJ7DA8GcATU-w3qepFDt66zT4uvtHZtAJRqWasB2WfOAQF1-q7gxPYjHvea_Z4K2BGR0fHRBCZSXS1cghbOfspcwylIVn7KSQVjyP_MQuMi7j_9q');

    sender.send(message, { registrationTokens: registrationTokens }, 10, function (err, response) {
        if (err) console.error(err);
        else console.log(response);
    });
}